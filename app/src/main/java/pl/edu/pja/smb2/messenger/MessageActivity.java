package pl.edu.pja.smb2.messenger;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MessageActivity extends AppCompatActivity {

    private static final String TAG = MessageActivity.class.getSimpleName();
    public static final String ACTION = "pl.edu.pja.smbmessengerreceiver.send";
    public static final String PERMISSION = "pl.edu.pja.smbmessenger.permission";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        final EditText editText = (EditText) findViewById(R.id.message_box);
        Button button = (Button) findViewById(R.id.send_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editText.getText())) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(MessageActivity.this);
                    alert.setMessage(R.string.empty_field_alert);
                    alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(ACTION);
                    intent.putExtra(Intent.EXTRA_TEXT, editText.getText().toString());
                    sendBroadcast(intent, PERMISSION);
                    Log.d(TAG, "onClick: intent sent");
                }
            }
        });
    }
}
